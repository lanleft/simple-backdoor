#include<stdio.h>
#include<time.h>
#include<string.h>
#include<windows.h>

int main(int argc, char* argv[])
{
	srand(time(NULL));
	int a = rand();
	int b = time(NULL);
	char beacon[200];
	char buffer[MAX_COMPUTERNAME_LENGTH + 1];
	DWORD size, len;
	HANDLE hFile ;
	BOOL error;
	size = sizeof(buffer);
	GetComputerNameA(buffer,&size);
	
	sprintf(beacon, "%u_%u_%s", a,b,buffer);
	len = strlen(beacon);
	
	hFile = CreateFileA("beacon.txt",GENERIC_WRITE,0,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		printf("Unable to create/open file\n");
	}
	error = WriteFile(hFile, beacon, len, NULL, NULL);
	if(error == FALSE)
	{
		printf("Unable to write to file\n");
	}
	else
	{
		printf("Beacon is ready\n");
	}
	CloseHandle(hFile);
	return 0;
}
